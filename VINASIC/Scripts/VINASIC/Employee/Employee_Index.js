﻿if (typeof VINASIC == "undefined" || !VINASIC) {
    var VINASIC = {};
}
VINASIC.namespace = function () {
    var a = arguments,
        o = null,
        i, j, d;
    for (i = 0; i < a.length; i = i + 1) {
        d = ("" + a[i]).split(".");
        o = VINASIC;
        for (j = (d[0] === "VINASIC") ? 1 : 0; j < d.length; j = j + 1) {
            o[d[j]] = o[d[j]] || {};
            o = o[d[j]];
        }
    }
    return o;
};
VINASIC.namespace("Employee");
VINASIC.Employee = function () {
    var global = {
        UrlAction: {
            GetListEmployee: "/Employee/GetEmployees",
            SaveEmployee: "/Employee/SaveEmployee",
            DeleteEmployee: "/Employee/DeleteEmployee"
        },
        Element: {
            JtableEmployee: "jtableEmployee",
            PopupEmployee: "popup_Employee",
            PopupSearch: "popup_SearchEmployee"
        },
        Data: {
            ModelEmployee: {},
            ModelConfig: {},
            ClientId: ""
        }
    };
    this.GetGlobal = function () {
        return global;
    };
    function reloadListEmployee() {
        var keySearch = $("#txtSearch").val();
        $("#" + global.Element.JtableEmployee).jtable("load", { 'keyword': keySearch });
    }
    /*function init model using knockout Js*/
    function initViewModel(employee) {
        var employeeViewModel = {
            Id: 0,
            Name: "",
            Address: "",
            Mobile: "",
            Email: "",
            PositionName: "",
            HireDate:""
        };
        if (employee != null) {
            employeeViewModel = {
                Id: ko.observable(employee.Id),
                Name: ko.observable(employee.Name),
                Address: ko.observable(employee.Address),
                Mobile: ko.observable(employee.Mobile),
                Email: ko.observable(employee.Email),
                PositionName: ko.observable(employee.PositionName),
                HireDate: ko.observable(employee.HireDate)
            };
        }
        return employeeViewModel;
    }
    function bindData(employee) {
        global.Data.ModelEmployee = initViewModel(employee);
        ko.applyBindings(global.Data.ModelEmployee);
    }
    /*end function*/

    /*function show Popup*/
    function showPopupEmployee() {
        $("#" + global.Element.PopupEmployee).modal("show");
    }
    /*End*/

    /*function Delete */
    function deleteRow(id) {
        $.ajax({
            url: global.UrlAction.DeleteEmployee,
            type: 'POST',
            data: JSON.stringify({ 'id': id }),
            contentType: 'application/json charset=utf-8',
            success: function (data) {
                GlobalCommon.CallbackProcess(data, function () {
                    if (data.Result === "OK") {
                        reloadListEmployee();
                        toastr.success('Xóa Thành Công');
                    }
                }, false, global.Element.PopupEmployee, true, true, function () {

                    var msg = GlobalCommon.GetErrorMessage(data);
                    GlobalCommon.ShowMessageDialog(msg, function () { }, "Đã có lỗi xảy ra.");
                });
            }
        });
    }
    /*End Delete */
    function initComboBoxPosition() {
        var url = "/Employee/GetEmployeePosition";
        $.getJSON(url, function (datas) {
            $('#position').empty();
            if (datas.length > 0) {
                for (var i = 0; i < datas.length; i++) {
                    $('#position').append('<option value="' + datas[i].Value + '">' + datas[i].Text + '</option>');
                }
            }
            else {
                $('#position').append('<option value="0">Không Có Dữ Liệu </option>');
            }
        });
    }
    /*function Init List Using Jtable */
    function initListEmployee() {
        $("#" + global.Element.JtableEmployee).jtable({
            title: "Danh Sách Nhân Viên",
            paging: true,
            pageSize: 10,
            pageSizeChangeEmployee: true,
            sorting: true,
            selectShow: true,
            actions: {
                listAction: global.UrlAction.GetListEmployee,
                createAction: global.Element.PopupEmployee,
                createObjDefault: initViewModel(null),
                searchAction: global.Element.PopupSearch
            },
            messages: {
                addNewRecord: "Thêm mới",
                searchRecord: "Tìm kiếm",
                selectShow: "Ẩn hiện cột"
            },
            fields: {
                Id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                Name: {
                    visibility: "fixed",
                    title: "Tên Loại",
                    width: "20%",
                    display: function (data) {
                        var text = $("<a href=\"#\" class=\"clickable\" title=\"Chỉnh sửa thông tin.\">" + data.record.Name + "</a>");
                        text.click(function () {
                            data.record.HireDate = FormatDateJsonToString(data.record.HireDate, "yyyy-mm-dd");
                            $('#date').val(data.record.HireDate);
                            $('#position').val(data.record.PositionId);
                            bindData(data.record);
                            showPopupEmployee();
                        });
                        return text;
                    }
                },
                Address: {
                    title: "Địa Chỉ",
                    width: "20%"
                },
                Mobile: {
                    title: "Số Điện Thoại",
                    width: "10%"
                },
                Email: {
                    title: "Email",
                    width: "10%"
                },
                PositionName: {
                    title: "Chức Vụ",
                    width: "10%"
                },
                HireDate: {
                    title: 'Ngày Vào Làm',
                    width: "10%",
                    type: 'date',
                    displayFormat: 'dd-mm-yy'
                },
                Delete: {
                    title: "Xóa",
                    width: "5%",
                    sorting: false,
                    display: function (data) {
                        var text = $('<button title="Xóa" class="jtable-command-button jtable-delete-command-button"><span>Xóa</span></button>');
                        text.click(function () {
                            GlobalCommon.ShowConfirmDialog("Bạn có chắc chắn muốn xóa?", function () {
                                deleteRow(data.record.Id);
                                var realTimeHub = $.connection.realTimeJTableDemoHub;
                                realTimeHub.server.sendUpdateEvent("jtableEmployee");
                                $.connection.hub.start();
                            }, function () { }, "Đồng ý", "Hủy bỏ", "Thông báo");
                        });
                        return text;
                    }
                }
            }
        });
    }
    /*End init List */

    /*function Check Validate */
    function checkValidate() {
        if ($('#Name').val().trim() === "") {
            GlobalCommon.ShowMessageDialog("Vui lòng nhập Tên Ngân Hàng.", function () { }, "Lỗi Nhập liệu");
            $("#Name").focus();
            return false;
        }
        return true;
    }
    /*End Check Validate */

    /*function Save */
    function saveEmployee() {
        global.Data.ModelEmployee.PositionId = $('#position').val();
        $.ajax({
            url: global.UrlAction.SaveEmployee,
            type: 'post',
            data: ko.toJSON(global.Data.ModelEmployee),
            contentType: 'application/json',
            success: function (result) {
                $('#loading').hide();
                GlobalCommon.CallbackProcess(result, function () {
                    if (result.Result === "OK") {
                        bindData(null);
                        reloadListEmployee();
                        $("#popup_Employee").modal("hide");
                        toastr.success("Thành Công");
                        var realTimeHub = $.connection.realTimeJTableDemoHub;
                        realTimeHub.server.sendUpdateEvent("jtableEmployee", global.Data.ClientId, "Cập nhật Nhân Viên");
                        $.connection.hub.start();
                    }
                }, false, global.Element.PopupEmployee, true, true, function () {
                    var msg = GlobalCommon.GetErrorMessage(result);
                    GlobalCommon.ShowMessageDialog(msg, function () { }, "Đã có lỗi xảy ra trong quá trình sử lý.");
                });
            }
        });
    }
    /*End Save */
    /* Region Register and init bootrap Popup*/
    function initPopupEmployee() {
        $("#" + global.Element.PopupEmployee).modal({
            keyboard: false,
            show: false
        });
        $("#" + global.Element.PopupEmployee + " button[save]").click(function () {
            saveEmployee();
            var realTimeHub = $.connection.realTimeJTableDemoHub;
            realTimeHub.server.sendUpdateEvent("jtableEmployee", global.Data.ClientId, "Cập nhật loại dịch vụ");
            $.connection.hub.start();
        });
        $("#" + global.Element.PopupEmployee + " button[cancel]").click(function () {
            $("#" + global.Element.PopupEmployee).modal("hide");
        });
    }
    /*End bootrap*/
    /* Region Register and init*/
    this.reloadListEmployee = function () {
        reloadListEmployee();
    };
    this.initViewModel = function (employee) {
        initViewModel(employee);
    };
    this.bindData = function (employee) {
        bindData(employee);
    };
    var registerEvent = function () {
        $("[cancel]").click(function () {
            bindData(null);
        });
    };
    this.Init = function () {
        registerEvent();
        initComboBoxPosition();
        initListEmployee();
        reloadListEmployee();
        initPopupEmployee();
        bindData(null);
    };
};
/*End Region*/
$(document).ready(function () {
    var employee = new VINASIC.Employee();
    employee.Init();
});