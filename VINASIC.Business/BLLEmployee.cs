﻿using System;
using System.Collections.Generic;
using System.Linq;
//using GPRO.Core.Mvc;
using GPRO.Ultilities;
using Dynamic.Framework;
using Dynamic.Framework.Infrastructure.Data;
using Dynamic.Framework.Mvc;
using PagedList;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;
using VINASIC.Data;
using VINASIC.Data.Repositories;
using VINASIC.Object;

namespace VINASIC.Business
{
    public class BllEmployee : IBllEmployee
    {
        private readonly IT_EmployeeRepository _repEmployee;
        private readonly IT_CustomerRepository _repCustomer;
        private readonly IT_OrganizationRepository _repOrganizationRepository;
        private readonly IT_PositionRepository _repPositionRepository;
        private readonly IT_OrderDetailRepository _repOrderDetailRepository;
        private readonly IT_OrderRepository _repOrderRepository;
        private readonly IUnitOfWork<VINASICEntities> _unitOfWork;
        public BllEmployee(IUnitOfWork<VINASICEntities> unitOfWork, IT_EmployeeRepository repEmployee, IT_OrderDetailRepository repOrderDetailRepository, IT_CustomerRepository repCustomer, IT_OrderRepository repOrderRepository, IT_OrganizationRepository repOrganizationRepository, IT_PositionRepository repPositionRepository)
        {
            _unitOfWork = unitOfWork;
            _repEmployee = repEmployee;
            _repCustomer = repCustomer;
            _repOrganizationRepository = repOrganizationRepository;
            _repOrderDetailRepository = repOrderDetailRepository;
            _repOrderRepository = repOrderRepository;
            _repPositionRepository = repPositionRepository;
        }
        private void SaveChange()
        {

            try
            {
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private bool CheckEmployeeName(string employeeName, int id)
        {
            var checkResult = false;
            try
            {
                var checkName = _repEmployee.GetMany(c => !c.IsDeleted && c.Id != id && c.Name.Trim().ToUpper().Equals(employeeName.Trim().ToUpper())).FirstOrDefault();
                if (checkName == null)
                    checkResult = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return checkResult;
        }

        public ResponseBase Create(ModelEmployee obj)
        {
            var allPos = _repPositionRepository.GetMany(x => !x.IsDeleted).ToList();

            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                if (obj != null)
                {           
                    if (CheckEmployeeName(obj.Name, obj.Id))
                    {
                        obj.OrganizationId = allPos.Where(x=>x.Id==obj.Id).Select(x=>x.OrganizationId).FirstOrDefault();
                        var employee = new T_Employee();
                        Parse.CopyObject(obj, ref employee);
                        employee.CreatedDate = DateTime.Now;
                        _repEmployee.Add(employee);
                        SaveChange();
                        result.IsSuccess = true;
                    }
                    else
                    {
                        result.IsSuccess = false;
                    }
                }
                else
                {
                    result.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseBase Update(ModelEmployee obj)
        {
            var allPos = _repPositionRepository.GetMany(x => !x.IsDeleted).ToList();
            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                if (!CheckEmployeeName(obj.Name, obj.Id))
                {
                    result.IsSuccess = false;
                }
                else
                {
                    var employee = _repEmployee.Get(x => x.Id == obj.Id && !x.IsDeleted);
                    if (employee != null)
                    {
                        obj.OrganizationId = allPos.Where(x => x.Id == obj.Id).Select(x => x.OrganizationId).FirstOrDefault();
                        employee.Name = obj.Name;
                        employee.Address = obj.Address;
                        employee.Mobile = obj.Mobile;
                        employee.Email = obj.Email;
                        employee.HireDate = obj.HireDate;
                        employee.PositionId = obj.PositionId;
                        employee.UpdatedDate = DateTime.Now;
                        employee.UpdatedUser = obj.UpdatedUser;
                        _repEmployee.Update(employee);
                        SaveChange();
                        result.IsSuccess = true;
                    }
                    else
                    {
                        result.IsSuccess = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public ResponseBase DeleteById(int id, int userId)
        {
            ResponseBase responResult;

            try
            {
                responResult = new ResponseBase();
                var employee = _repEmployee.GetMany(c => !c.IsDeleted && c.Id == id).FirstOrDefault();
                if (employee != null)
                {

                    
                    employee.IsDeleted = true;
                    employee.DeletedUser = userId;
                    employee.DeletedDate = DateTime.Now;
                    _repEmployee.Update(employee);
                    SaveChange();
                    responResult.IsSuccess = true;
                }
                else
                {
                    responResult.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return responResult;
        }
        public ResponseBase DeleteByListId(List<int> listId, int userId)
        {
            ResponseBase responResult = null;
            try
            {
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return responResult;
        }
        public PagedList<ModelEmployee> GetList(string keyWord, int startIndexRecord, int pageSize, string sorting)
        {
            try
            {
                if (string.IsNullOrEmpty(sorting))
                {
                    sorting = "CreatedDate DESC";
                }
                var employees = _repEmployee.GetMany(c => !c.IsDeleted).Select(c => new ModelEmployee()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Address = c.Address,
                    Mobile = c.Mobile,
                    Email = c.Email,
                    HireDate = c.HireDate,
                    PositionId = c.PositionId,
                    PositionName=c.T_Position.Name,
                    CreatedDate = c.CreatedDate,
                }).OrderBy(sorting).ToList();
                var pageNumber = (startIndexRecord / pageSize) + 1;
                return new PagedList<ModelEmployee>(employees, pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ModelSelectItem> GetListEmployee()
        {
            List<ModelSelectItem> listModelSelect = new List<ModelSelectItem>
            {
                new ModelSelectItem() {Value = 0, Name = "---Chọn Hết----"}
            };

            try
            {
                listModelSelect.AddRange(_repEmployee.GetMany(x => !x.IsDeleted && (x.T_Organization.Id == 1 || x.T_Organization.Id == 2)).Select(x => new ModelSelectItem() { Value = x.Id, Name = x.Name }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listModelSelect;
        }

        public List<ModelSelectItem> GetCustomerByOrganization(int organId)
        {
            List<ModelSelectItem> listModelSelect = new List<ModelSelectItem>
            {
                new ModelSelectItem() {Value = 0, Name = "---Chọn----"}
            };
            try
            {
                listModelSelect.AddRange(_repEmployee.GetMany(x => !x.IsDeleted && x.OrganizationId == organId).Select(x => new ModelSelectItem() { Value = x.Id, Name = x.Name }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listModelSelect;
        }

        public PagedList<ModelForDesign> GetListForDesign(string keyWord, int startIndexRecord, int pageSize, string sorting,int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(sorting))
                {
                    sorting = "CreatedDate DESC";
                }
                var listDesignProcess = _repOrderDetailRepository.GetMany(c => !c.IsDeleted && c.DesignUser == userId).Select(c => new ModelForDesign()
                {
                    CustomerName = c.T_Order.Name,
                    Id = c.Id,
                    CommodityName = c.CommodityName,
                    FileName = c.FileName,
                    Height = c.Height,
                    Width = c.Width,
                    DesignFrom = c.DesignFrom,
                    DesignTo = c.DesignTo,
                    DesignStatus = c.DesignStatus??0,
                    DesignDescription = c.DesignDescription,
                    StrdesignStatus = c.DesignStatus == null ? "Không Xử Lý" : (c.DesignStatus == 1 ? "Đang Xử Lý" : (c.DesignStatus == 2 ? "Đã Xử Lý" : "Không xử lý")),
                    CreatedDate = c.CreatedDate,
                }).OrderBy(sorting).ToList();
                var pageNumber = (startIndexRecord / pageSize) + 1;
                return new PagedList<ModelForDesign>(listDesignProcess, pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PagedList<ModelForPrint> GetListForPrint(string keyWord, int startIndexRecord, int pageSize, string sorting,int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(sorting))
                {
                    sorting = "CreatedDate DESC";
                }
                var listPrintProcess = _repOrderDetailRepository.GetMany(c => !c.IsDeleted && c.PrintUser == userId).Select(c => new ModelForPrint()
                {
                    CustomerName = c.T_Order.Name,
                    Id = c.Id,
                    PrintStatus = c.PrintStatus??0,
                    CommodityName = c.CommodityName,
                    FileName = c.FileName,
                    Height = c.Height,
                    Width = c.Width,
                    PrintDescription = c.PrintDescription,
                    PrintFrom = c.PrintFrom,
                    PrintTo = c.PrintTo,
                    StrPrintStatus = c.PrintStatus == null ? "Không Xử Lý" : (c.PrintStatus == 1 ? "Đang Xử Lý" :(c.PrintStatus == 2? "Đã Xử Lý":"Không xử lý")),
                    CreatedDate = c.CreatedDate,
                }).OrderBy(sorting).ToList();
                var pageNumber = (startIndexRecord / pageSize) + 1;
                return new PagedList<ModelForPrint>(listPrintProcess, pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseBase DesignUpdateOrderDeatail(int id, int stautus,int userId)
        {
            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                if (stautus >= 2)
                {
                    result.IsSuccess = false;
                    result.Data = "Đã Hoàn Thành Không Thể Cập nhật";
                    return result;
                }
                var orderDetail = _repOrderDetailRepository.Get(x => x.Id == id && !x.IsDeleted);     
                orderDetail.DesignStatus = stautus + 1;
                orderDetail.UpatedDate = DateTime.Now;
                orderDetail.UpdatedUser = userId;
                _repOrderDetailRepository.Update(orderDetail);
                SaveChange();
                result.IsSuccess = true;
                result.Data = DateTime.Now;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public ResponseBase PrintUpdateOrderDeatail(int id, int stautus,int userId)
        {
            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                if (stautus >= 2)
                {
                    result.IsSuccess = false;
                    result.Data = "Đã Hoàn Thành Không Thể Cập nhật";
                    return result;
                }
                var orderDetail = _repOrderDetailRepository.Get(x => x.Id == id && !x.IsDeleted);                
                orderDetail.PrintStatus = stautus + 1;
                orderDetail.UpatedDate = DateTime.Now;
                orderDetail.UpdatedUser = userId;
                _repOrderDetailRepository.Update(orderDetail);
                SaveChange();
                result.IsSuccess = true;
                result.Data = DateTime.Now;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public ResponseBase BusinessUpdateOrderDeatail(int id, int employeeId,string description,int type,int stautus,int userId)
        {
            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                var orderDetail = _repOrderDetailRepository.Get(x => x.Id == id && !x.IsDeleted);
                if (type==1)
                {
                    orderDetail.DesignStatus = stautus;
                    orderDetail.DesignUser = employeeId;
                    orderDetail.DesignDescription = description;
                }
                else
                {
                    orderDetail.PrintStatus = stautus;
                    orderDetail.PrintUser = employeeId;
                    orderDetail.PrintDescription = description;
                }
                orderDetail.UpdatedUser = userId;
                orderDetail.UpatedDate = DateTime.Now;
                _repOrderDetailRepository.Update(orderDetail);
                SaveChange();
                result.IsSuccess = true;
                result.Data = DateTime.Now;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}

