﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dynamic.Framework.Mvc;
using Sysnify.Common;
using Sysnify.Common.DataPath;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;
using VINASIC.Object;

namespace VINASIC.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IBllOrder _bllOrder;
        private readonly IBllProductType _bllProductType;
        private readonly IBllProduct _bllProduct;
        private readonly IBllEmployee _bllEmployee;
        private readonly IBllCustomer _bllCustomer;

        public OrderController(IBllOrder bllOrder, IBllEmployee bllEmployee, IBllCustomer bllCustomer, IBllProductType bllProductType, IBllProduct bllProduct)
        {
            _bllOrder = bllOrder;
            _bllEmployee = bllEmployee;
            _bllCustomer = bllCustomer;
            _bllProductType = bllProductType;
            _bllProduct = bllProduct;
        }
        public ActionResult Index()
        {
            try
            {
                var listModelSelect = _bllEmployee.GetListEmployee();
                List<SelectListItem> listEmployee = new List<SelectListItem>();
                if (listModelSelect != null && listModelSelect.Count > 0)
                {
                    listEmployee = listModelSelect.Select(c => new SelectListItem
                    {
                        Text = c.Name,
                        Value = c.Value.ToString(),
                    }).ToList();
                }
                ViewBag.listEmployee = listEmployee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        [System.Web.Mvc.HttpPost]
        public JsonResult GetOrders(int jtStartIndex = 0, int jtPageSize = 10, string jtSorting = "", string keyword = "", string fromDate = "", string toDate = "")
        {
            try
            {               
                var listOrder = _bllOrder.GetList(UserContext.IsOwner ? UserContext.UserID : 1, jtStartIndex, jtPageSize, jtSorting, fromDate, toDate);
                JsonDataResult.Records = listOrder;
                JsonDataResult.Result = "OK";
                JsonDataResult.TotalRecordCount = listOrder.TotalItemCount;

            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }
        public JsonResult ListOrderDetail(int orderId)
        {
            try
            {
                Thread.Sleep(200);
                var orderDetail = _bllOrder.GetListOrderDetailByOrderId(orderId);
                return Json(new { Result = "OK", Records = orderDetail });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult UpdateHasTax(int id, int hasTax)
        {
            try
            {
                //if (isAuthenticate)
                //{
                var responseResult = _bllOrder.UpdateHasTax(id, hasTax, UserId);
                if (responseResult.IsSuccess)
                    JsonDataResult.Result = "OK";
                else
                {
                    JsonDataResult.Result = "ERROR";
                    JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                }
                //}
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult UpdatePayment(int id, int payment)
        {
            try
            {
                //if (isAuthenticate)
                //{
                var responseResult = _bllOrder.UpdatePayment(id, payment, UserId);
                if (responseResult.IsSuccess)
                    JsonDataResult.Result = "OK";
                else
                {
                    JsonDataResult.Result = "ERROR";
                    JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                }
                //}
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult UpdateApproval(int id, int approval)
        {
            try
            {
                //if (isAuthenticate)
                //{
                var responseResult = _bllOrder.UpdateApproval(id, approval, UserId);
                if (responseResult.IsSuccess)
                    JsonDataResult.Result = "OK";
                else
                {
                    JsonDataResult.Result = "ERROR";
                    JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                }
                //}
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        [System.Web.Mvc.HttpPost]
        public JsonResult SaveOrder(int orderId,int employeeId, int customerId, string customerName, string customerPhone, string customerMail, string customerAddress, string customerTaxCode, string dateDelivery,float orderTotal, List<ModelDetail> listDetail)
        {
            try
            {
                var saveOrder = new ModelSaveOrder
                {
                    OrderId = orderId,
                    EmployeeId = employeeId,
                    OrderTotal = orderTotal,
                    CustomerId = customerId,
                    CustomerName = customerName,
                    CustomerPhone = customerPhone,
                    CustomerMail = customerMail,
                    CustomerAddress = customerAddress,
                    CustomerTaxCode = customerTaxCode,
                    DateDelivery = DateTime.Parse(dateDelivery ?? DateTime.Now.ToString(CultureInfo.InvariantCulture)),
                    Detail = listDetail
                };
                if (!IsAuthenticate)
                {
                    var responseResult = saveOrder.OrderId == 0 ? _bllOrder.CreateOrder(saveOrder,UserContext.UserID) : _bllOrder.UpdatedOrder(saveOrder,UserContext.UserID);
                    if (!responseResult.IsSuccess)
                    {
                        JsonDataResult.Result = "ERROR";
                        JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                    }
                    else
                    {
                        JsonDataResult.Result = "OK";
                    }
                }
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update ", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        [System.Web.Mvc.HttpPost]
        public JsonResult GetAllCustomer()
        {
            try
            {
                Thread.Sleep(200);
                var customers = _bllCustomer.GetAllCustomerName();
                return Json(new { Result = "OK", Records = customers });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetListProductType()
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _bllProductType.GetListProductType();
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult GetListProduct(int productType)
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _bllProduct.GetListProduct(productType);
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult GetCustomerById(int customerId)
        {
            try
            {
                Thread.Sleep(200);
                var customer = _bllCustomer.GetCustomerById(customerId);
                return Json(new { Result = "OK", Records = customer });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetCustomerByName(string customerName)
        {
            try
            {
                Thread.Sleep(200);
                var customer = _bllCustomer.GetCustomerByName(customerName);
                return Json(new { Result = "OK", Records = customer });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetCustomerByPhone(string phoneNumber)
        {
            try
            {
                Thread.Sleep(200);
                var customer = _bllCustomer.GetCustomerByPhone(phoneNumber);
                return Json(new { Result = "OK", Records = customer });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetCustomerByOrganization(int orgId)
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _bllEmployee.GetCustomerByOrganization(orgId);
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult UpdateDesignUser(int detailId, int employeeId,string description)
        {
            try
            {
                //if (isAuthenticate)
                //{
                var responseResult = _bllOrder.UpdateDesignUser(detailId, employeeId, description);
                if (responseResult.IsSuccess)
                    JsonDataResult.Result = "OK";
                else
                {
                    JsonDataResult.Result = "ERROR";
                    JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                }
                //}
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult UpdateHaspay(int orderId,string haspay)
        {
            try
            {
                //if (isAuthenticate)
                //{
                var responseResult = _bllOrder.UpdateHaspay(orderId, haspay);
                if (responseResult.IsSuccess)
                    JsonDataResult.Result = "OK";
                else
                {
                    JsonDataResult.Result = "ERROR";
                    JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                }
                //}
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult UpdatePrintUser(int detailId, int employeeId,string description)
        {
            try
            {
                //if (isAuthenticate)
                //{
                var responseResult = _bllOrder.UpdatePrintUser(detailId, employeeId, description);
                if (responseResult.IsSuccess)
                    JsonDataResult.Result = "OK";
                else
                {
                    JsonDataResult.Result = "ERROR";
                    JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                }
                //}
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }


        [System.Web.Mvc.HttpGet]
        public HttpResponseMessage ExportReport([FromUri] DateTime fromDate, [FromUri]DateTime toDate)
        {
            var frDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0, 0);
            var tDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59, 999);
            var lst = _bllOrder.ExportReport(frDate, tDate);
            GridView gv = new GridView();
            gv.DataSource = lst.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            htw.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            System.Web.HttpContext.Current.Response.Close();
            return null;
        }

    }
}
