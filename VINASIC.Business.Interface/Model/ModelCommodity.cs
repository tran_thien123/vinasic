﻿using VINASIC.Object;

namespace VINASIC.Business.Interface.Model
{
    public class ModelCommodity : T_Commodity
    {
        public string CommodityTypeName { get; set; }
    }
}

