﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dynamic.Framework;
using Dynamic.Framework.Infrastructure.Data;
using PagedList;
using VINASIC.Data;
using VINASIC.Data.Repositories;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;
using VINASIC.Object;

namespace SystemAccount.Bussiness
{
    public class BLLPermission : IBLLPermission
    {
        private readonly IT_PermissionRepository repPermission;
        private readonly IUnitOfWork<VINASICEntities> unitOfWork;
        private readonly IBLLUserRole bllUserRole;
        private readonly IBLLRolePermission bllRolePermission;
        public BLLPermission(IUnitOfWork<VINASICEntities> _unitOfWork, IT_PermissionRepository _repPermission, IBLLUserRole _bllUserRole, IBLLRolePermission _bllRolePermission)
        {
            this.unitOfWork = _unitOfWork;
            this.repPermission = _repPermission;
            this.bllUserRole = _bllUserRole;
            this.bllRolePermission = _bllRolePermission;
        }

        public ResponseBase Create(T_Permission asset)
        {
            throw new NotImplementedException();
        }

        public ResponseBase Update(T_Permission asset)
        {
            throw new NotImplementedException();
        }

        public ResponseBase DeleteById(int id, int userId)
        {
            throw new NotImplementedException();
        }

        public ResponseBase DeleteByListId(List<int> listId, int userId)
        {
            throw new NotImplementedException();
        }

        public PagedList<T_Permission> GetList(int counTryId, int startIndexRecord, int pageSize, string sorting)
        {
            throw new NotImplementedException();
        }

        public ResponseBase GetListPermissionByListRoleId(List<int> listRoleId)
        {
            ResponseBase responResult = null;
            try
            {
                //var listPermission = repPermission.GetMany(x=>x.)
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            return responResult;
        }
        public List<T_Permission> GetListPermissionByFeatureID(int FeatureId)
        {
            List<T_Permission> listPermisson = null;
            try
            {
                 listPermisson = repPermission.GetMany(x => !x.IsDeleted && x.FeatureId == FeatureId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listPermisson;
        }
    }
}
