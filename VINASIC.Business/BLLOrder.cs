﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using Dynamic.Framework;
using Dynamic.Framework.Infrastructure.Data;
using Dynamic.Framework.Mvc;
using PagedList;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;
using VINASIC.Data;
using VINASIC.Data.Repositories;
using VINASIC.Business.Interface.Enum;
using VINASIC.Object;

namespace VINASIC.Business
{
    public class BllOrder : IBllOrder
    {
        private readonly IT_OrderRepository _repOrder;
        private readonly IT_CustomerRepository _repCus;
        private readonly IT_UserRepository _repUserRepository;
        private readonly IT_EmployeeRepository _repEmp;
        private readonly IT_OrderDetailRepository _repOrderDetail;
        private readonly IUnitOfWork<VINASICEntities> _unitOfWork;
        public BllOrder(IUnitOfWork<VINASICEntities> unitOfWork, IT_OrderRepository repOrder, IT_OrderDetailRepository repOrderDetail, IT_CustomerRepository repCus, IT_EmployeeRepository repEmp, IT_UserRepository repUserRepository)
        {
            _unitOfWork = unitOfWork;
            _repOrder = repOrder;
            _repOrderDetail = repOrderDetail;
            _repUserRepository = repUserRepository;
            _repCus = repCus;
            _repEmp = repEmp;
        }
        private void SaveChange()
        {
            _unitOfWork.Commit();
        }

        public PagedList<ModelOrder> GetList(int employee, int startIndexRecord, int pageSize, string sorting,string fromDate,string toDate)
        {
            if (string.IsNullOrEmpty(sorting))
            {
                sorting = "CreatedDate DESC";
            }
            if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                var realfromDate = DateTime.Parse(fromDate);
                var realtoDate = DateTime.Parse(toDate);
                var frDate = new DateTime(realfromDate.Year, realfromDate.Month, realfromDate.Day, 0, 0, 0, 0);
                var tDate = new DateTime(realtoDate.Year, realtoDate.Month, realtoDate.Day, 23, 59, 59, 999);
                var orders = _repOrder.GetMany(c => !c.IsDeleted && c.CreatedDate >= frDate && c.CreatedDate <= tDate).Select(c => new ModelOrder()
                {
                    CustomerPhone = c.T_Customer.Mobile,
                    CustomerEmail = c.T_Customer.Email,
                    CustomerAddress = c.T_Customer.Address,
                    CustomerTaxCode = c.T_Customer.TaxCode,
                    CreatedForUser = c.CreatedForUser,
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description,
                    CustomerId = c.CustomerId,
                    DeliveryDate = c.DeliveryDate,
                    SubTotal = c.SubTotal,
                    IsPayment = c.IsPayment,
                    IsApproval = c.IsApproval,
                    HasTax = c.HasTax,
                    strIspayment = c.IsPayment ? "Rồi" : "Chưa",
                    strIsApproval = c.IsApproval ? "Rồi" : "Chưa",
                    strHasTax = c.HasTax == null ? "Có" : "Không",
                    CreatedUser = c.CreatedUser,
                    CreateUserName = c.T_Employee.Name,
                    CreatedDate = c.CreatedDate,
                    HasPay = c.HasPay??0,
                    T_OrderDetail = c.T_OrderDetail
                }).OrderBy(sorting).ToList();
                foreach (var order in orders)
                {
                    order.strHaspay = String.Format("{0:0,0}", order.HasPay??0);
                    order.strSubTotal = String.Format("{0:0,0}", order.SubTotal);
                    order.PrintfPecent = Math.Round(PrintfPecent(order.Id), 2);
                }
                if (employee != 1)
                    orders = orders.Where(c => c.CreatedUser == employee).ToList();
                var pageNumber = (startIndexRecord / pageSize) + 1;
                return new PagedList<ModelOrder>(orders, pageNumber, pageSize);
            }
            else
            {
                var orders = _repOrder.GetMany(c => !c.IsDeleted).Select(c => new ModelOrder()
                {
                    CustomerPhone = c.T_Customer.Mobile,
                    CustomerEmail = c.T_Customer.Email,
                    CustomerAddress = c.T_Customer.Address,
                    CustomerTaxCode = c.T_Customer.TaxCode,
                    CreatedForUser = c.CreatedForUser,
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description,
                    CustomerId = c.CustomerId,
                    DeliveryDate = c.DeliveryDate,
                    SubTotal = c.SubTotal,
                    IsPayment = c.IsPayment,
                    IsApproval = c.IsApproval,
                    HasTax = c.HasTax,
                    strIspayment = c.IsPayment ? "Rồi" : "Chưa",
                    strIsApproval = c.IsApproval ? "Rồi" : "Chưa",
                    strHasTax = c.HasTax == null ? "Có" : "Không",
                    CreatedUser = c.CreatedUser,
                    CreateUserName = c.T_Employee.Name,
                    CreatedDate = c.CreatedDate,
                    HasPay = c.HasPay??0,
                    T_OrderDetail = c.T_OrderDetail
                }).OrderBy(sorting).ToList();
                foreach (var order in orders)
                {
                    order.strHaspay = String.Format("{0:0,0}", order.HasPay??0);
                    order.strSubTotal = String.Format("{0:0,0}", order.SubTotal);
                    order.PrintfPecent = Math.Round(PrintfPecent(order.Id), 2);
                }
                if (employee != 1)
                    orders = orders.Where(c => c.CreatedUser == employee).ToList();
                var pageNumber = (startIndexRecord / pageSize) + 1;
                return new PagedList<ModelOrder>(orders, pageNumber, pageSize);
            }
            
        }

        public List<ModelOrderDetail> GetListOrderDetailByOrderId(int orderId)
        {
            var allEmployee = _repEmp.GetMany(x => !x.IsDeleted);
            
            var ordeDetails = _repOrderDetail.GetMany(o => !o.IsDeleted && o.OrderId == orderId).Select(o => new
                ModelOrderDetail()
            {
                Id = o.Id,
                OrderId = o.OrderId,
                CommodityId = o.CommodityId,
                CommodityName = o.T_Product.Name,
                PrintDescription = o.PrintDescription,
                DesignDescription = o.DesignDescription,
                Description = o.Description,
                Height = o.Height,
                Width = o.Width,
                Square = o.Height ?? 0 * o.Width ?? 0,
                Quantity = o.Quantity,
                Price = o.Price,
                PrintStatus = o.PrintStatus,
                DesignStatus = o.DesignStatus,
                PrintUser = o.PrintUser,
                PrintFrom = o.PrintFrom,
                PrintTo = o.PrintTo,
                DesignUser = o.DesignUser,
                DesignFrom = o.DesignFrom,
                DesignTo = o.DesignTo,
                SubTotal = o.SubTotal,
                IsCompleted = o.IsCompleted,
                strIsComplete = o.IsCompleted ? "Đã In" : "Chưa In",
                strDesignStatus = o.DesignStatus == null ? "Không Xử Lý" : (o.DesignStatus == 1 ? "Đang Xử Lý" : (o.DesignStatus == 2 ? "Đã Xử Lý" : "Không xử lý")),
                strPrinStatus = o.PrintStatus == null ? "Không Xử Lý" : (o.PrintStatus == 1 ? "Đang Xử Lý" : (o.PrintStatus == 2 ? "Đã Xử Lý" : "Không xử lý")),
                CreatedDate = o.CreatedDate,
            }).ToList();
            foreach (var order in ordeDetails)
            {               
                var firstOrDefault = allEmployee.FirstOrDefault(x => x.Id == order.DesignUser);
                order.DesignUserName = firstOrDefault != null ? firstOrDefault.Name : "";              
                var orDefault = allEmployee.FirstOrDefault(x => x.Id == order.PrintUser);
                order.PrintUserName = orDefault != null ? orDefault.Name : "";              
                order.strSubTotal = String.Format("{0:0,0}", order.SubTotal);
                order.strPrice = String.Format("{0:0,0}", order.Price);
            }
            return ordeDetails;
        }
        public ResponseBase CreateOrder(ModelSaveOrder obj, int userId)
        {
            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                if (obj != null)
                {
                    int baseCustomerId;
                    if (obj.CustomerId != 0)
                    {
                        var cus = _repCus.Get(x => x.Id == obj.CustomerId);
                        cus.Name = obj.CustomerName;
                        cus.Address = obj.CustomerAddress;
                        cus.Mobile = obj.CustomerPhone;
                        cus.Email = obj.CustomerMail;
                        cus.TaxCode = obj.CustomerTaxCode;
                        cus.UpdatedDate = DateTime.Now;
                        cus.UpdatedUser = userId;
                        baseCustomerId = obj.CustomerId;
                        _repCus.Update(cus);
                        SaveChange();
                    }
                    else
                    {
                        var cus = new T_Customer();
                        cus.Name = obj.CustomerName;
                        cus.Address = obj.CustomerAddress;
                        cus.Mobile = obj.CustomerPhone;
                        cus.Email = obj.CustomerMail;
                        cus.TaxCode = obj.CustomerTaxCode;
                        cus.CreatedDate = DateTime.Now;
                        cus.CreatedUser = userId;
                        _repCus.Add(cus);
                        SaveChange();
                        baseCustomerId = cus.Id;
                    }

                    var order = new T_Order();
                    order.Name = obj.CustomerName;
                    order.Description = "";
                    order.CustomerId = baseCustomerId;
                    order.DeliveryDate = obj.DateDelivery;
                    order.SubTotal = obj.OrderTotal;
                    order.IsPayment = false;
                    order.IsApproval = false;
                    order.IsDeleted = false;
                    order.CreatedForUser = obj.EmployeeId;
                    order.CreatedUser = userId;
                    order.CreatedDate = DateTime.Now;
                    _repOrder.Add(order);
                    SaveChange();
                    foreach (var detail in obj.Detail)
                    {
                        var orderDetail = new T_OrderDetail();
                        orderDetail.Index = detail.Index;
                        orderDetail.OrderId = order.Id;
                        orderDetail.CommodityId = int.Parse(detail.CommodityId);
                        orderDetail.CommodityName = detail.CommodityName;
                        orderDetail.Height = detail.Height;
                        orderDetail.Width = detail.Width;
                        orderDetail.Quantity = detail.Quantity;
                        orderDetail.Price = detail.Price;
                        orderDetail.SubTotal = detail.Subtotal;
                        orderDetail.Description = detail.Description;
                        orderDetail.IsCompleted = false;
                        orderDetail.IsDeleted = false;
                        orderDetail.CreatedUser = userId;
                        orderDetail.CreatedDate = DateTime.Now;
                        orderDetail.FileName = detail.FileName;
                        _repOrderDetail.Add(orderDetail);
                        SaveChange();
                    }

                    result.IsSuccess = true;

                }
                else
                {
                    result.IsSuccess = false;
                    result.Errors.Add(new Error() { MemberName = "Create ProductType", Message = "Đối Tượng Không tồn tại" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public ResponseBase UpdatedOrder(ModelSaveOrder obj, int userId)
        {

            ResponseBase result = new ResponseBase { IsSuccess = false };
            try
            {
                if (obj != null)
                {
                        var cus = _repCus.Get(x => x.Id == obj.CustomerId);
                        cus.Name = obj.CustomerName;
                        cus.Address = obj.CustomerAddress;
                        cus.Mobile = obj.CustomerPhone;
                        cus.Email = obj.CustomerMail;
                        cus.TaxCode = obj.CustomerTaxCode;
                        cus.UpdatedDate = DateTime.Now;
                        cus.UpdatedUser = userId;
                        _repCus.Update(cus);
                        SaveChange();
                    

                    var order  = _repOrder.Get(x => x.Id == obj.OrderId);
                    order.Name = obj.CustomerName;
                    order.Description = "";
                    order.SubTotal = obj.OrderTotal;
                    order.CustomerId = obj.CustomerId;
                    order.DeliveryDate = obj.DateDelivery;
                    order.IsPayment = false;
                    order.IsApproval = false;
                    order.IsDeleted = false;
                    order.CreatedForUser = obj.EmployeeId;
                    order.UpdatedUser = userId;
                    order.UpatedDate = DateTime.Now;
                    _repOrder.Update(order);
                    SaveChange();
                    var baseOrderDetail = _repOrderDetail.GetMany(x => x.OrderId == order.Id).ToList();
                    foreach (var detail in baseOrderDetail)
                    {
                        _repOrderDetail.Delete(detail);
                        SaveChange();
                    }
                    foreach (var detail in obj.Detail)
                    {
                        var orderDetail = new T_OrderDetail
                        {
                            OrderId = order.Id,
                            Index = detail.Index,
                            CommodityId = int.Parse(detail.CommodityId),
                             CommodityName = detail.CommodityName,
                            Height = detail.Height,
                            Width = detail.Width,
                            Quantity = detail.Quantity,
                            Price = detail.Price,
                            SubTotal = detail.Subtotal,
                            Description = detail.Description,
                            IsCompleted = false,
                            IsDeleted = false,
                            CreatedUser = userId,
                            CreatedDate = DateTime.Now,
                            FileName = detail.FileName
                        };
                        _repOrderDetail.Add(orderDetail);
                        SaveChange();
                    }
                    result.IsSuccess = true;

                }
                else
                {
                    result.IsSuccess = false;
                    result.Errors.Add(new Error() { MemberName = "Create ProductType", Message = "Đối Tượng Không tồn tại" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public double PrintfPecent(int orderId)
        {
            double pecent = 100;
            var listOrderDetail = _repOrderDetail.GetMany(o => !o.IsDeleted && o.OrderId == orderId && o.T_Product.Id == 1).ToList();
            if (listOrderDetail.Count > 0)
            {
                var listComplete = _repOrderDetail.GetMany(o => !o.IsDeleted && o.OrderId == orderId && o.T_Product.Id == 1 && o.IsCompleted).ToList();
                if (listComplete.Count > 0)
                {
                    double totalm2 = (from orderDetail in listOrderDetail where orderDetail.Height != 0 && orderDetail.Width != 0 && orderDetail.Quantity != 0 select orderDetail.Height ?? 0 * orderDetail.Width ?? 0 * orderDetail.Quantity ?? 0).Aggregate<double, double>(0, (current, total) => current + total);
                    double totalm2Complete = (from complete in listComplete where complete.Height != 0 && complete.Width != 0 && complete.Quantity != 0 select complete.Height ?? 0 * complete.Width ?? 0 * complete.Quantity ?? 0).Aggregate<double, double>(0, (current, total) => current + total);
                    pecent = totalm2Complete / totalm2 * 100;
                }
            }
            return pecent;
        }

        public ResponseBase UpdateApproval(int orderId, int id, int userId)
        {
            var responResult = new ResponseBase();
            var order = _repOrder.GetMany(c => !c.IsDeleted && c.Id == orderId).FirstOrDefault();
            if (order != null)
            {
                order.IsApproval = id != 1;
                order.UpdatedUser = userId;
                order.UpatedDate = DateTime.Now;
                _repOrder.Update(order);
                SaveChange();
                responResult.IsSuccess = true;
            }
            else
            {
                responResult.IsSuccess = false;
                responResult.Errors.Add(new Error() { MemberName = "Update", Message = "Lỗi" });
            }
            return responResult;
        }

        public ResponseBase UpdatePayment(int orderId, int id, int userId)
        {
            var responResult = new ResponseBase();
            var order = _repOrder.GetMany(c => !c.IsDeleted && c.Id == orderId).FirstOrDefault();
            if (order != null)
            {
                order.IsPayment = id != 1;
                order.UpdatedUser = userId;
                order.UpatedDate = DateTime.Now;
                _repOrder.Update(order);
                SaveChange();
                responResult.IsSuccess = true;
            }
            else
            {
                responResult.IsSuccess = false;
                responResult.Errors.Add(new Error() { MemberName = "Update", Message = "Lỗi" });
            }
            return responResult;
        }
        public ResponseBase UpdateHasTax(int orderId, int id, int userId)
        {
            var responResult = new ResponseBase();
            var order = _repOrder.GetMany(c => !c.IsDeleted && c.Id == orderId).FirstOrDefault();

            if (order != null)
            {
                var subTotal = order.SubTotal;
                if (id == 1)
                {
                    order.HasTax = false;
                    subTotal = subTotal - (subTotal * 10 / 110);
                }
                else
                {
                    order.HasTax = true;
                    subTotal = (subTotal * 10 / 100) + subTotal;

                }
                order.SubTotal = subTotal;
                order.UpdatedUser = userId;
                order.UpatedDate = DateTime.Now;
                _repOrder.Update(order);
                SaveChange();
                responResult.IsSuccess = true;
            }
            else
            {
                responResult.IsSuccess = false;
                responResult.Errors.Add(new Error() { MemberName = "Update", Message = "Lỗi" });
            }
            return responResult;
        }

        public ResponseBase UpdateDesignUser(int detailId, int designId,string description)
        {
            var responResult = new ResponseBase();
            var order = _repOrderDetail.GetMany(c => !c.IsDeleted && c.Id == detailId).FirstOrDefault();
            if (order != null)
            {
                order.DesignDescription = description;
                order.DesignUser = designId;
                order.UpatedDate = DateTime.Now;
                _repOrderDetail.Update(order);
                SaveChange();
                responResult.IsSuccess = true;
            }
            else
            {
                responResult.IsSuccess = false;
                responResult.Errors.Add(new Error() { MemberName = "Update", Message = "Lỗi" });
            }
            return responResult;
        }
        public ResponseBase UpdatePrintUser(int detailId, int printId,string description)
        {
            var responResult = new ResponseBase();
            var order = _repOrderDetail.GetMany(c => !c.IsDeleted && c.Id == detailId).FirstOrDefault();
            if (order != null)
            {
                order.PrintDescription = description;
                order.PrintUser = printId;
                order.UpatedDate = DateTime.Now;
                _repOrderDetail.Update(order);
                SaveChange();
                responResult.IsSuccess = true;
            }
            else
            {
                responResult.IsSuccess = false;
                responResult.Errors.Add(new Error() { MemberName = "Update", Message = "Lỗi" });
            }
            return responResult;
        }
        public ResponseBase UpdateHaspay( int orderId,string haspay)
        {
            var pay = string.IsNullOrEmpty(haspay) ? 0 : double.Parse(haspay);
            var responResult = new ResponseBase();
            var order = _repOrder.Get(c => !c.IsDeleted && c.Id == orderId);
            if (order != null)
            {
                order.HasPay = pay;
                order.UpatedDate = DateTime.Now;
                _repOrder.Update(order);
                SaveChange();
                responResult.IsSuccess = true;
            }
            else
            {
                responResult.IsSuccess = false;
                responResult.Errors.Add(new Error() { MemberName = "Update", Message = "Lỗi" });
            }
            return responResult;
        }

        public List<ModelExportOrder> ExportReport(DateTime fromDate, DateTime toDate)
        {
            var orders = _repOrderDetail.GetMany(c => !c.IsDeleted && c.CreatedDate >= fromDate && c.CreatedDate <= toDate).Select(c => new ModelExportOrder()
            {
               NgayTao  = c.CreatedDate,
               TenKhachHang = c.T_Order.Name,
               TenDichVu = c.CommodityName,
               MoTa = c.Description,
               ChieuDai = c.Width??0,
               ChieuRong = c.Height??0,
               DienTich = c.Height??0*c.Width??0,
               SoLuong = c.Quantity??0,
               DonGia = c.Price??0,
               ThanhTien = c.SubTotal,
            }).ToList();
            return orders;
        }
    }
}

