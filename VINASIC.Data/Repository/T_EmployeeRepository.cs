using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dynamic.Framework.Infrastructure.Data;
using VINASIC.Object;

namespace VINASIC.Data.Repositories
{
    public partial class T_EmployeeRepository : RepositoryBase<VINASICEntities,T_Employee> , IT_EmployeeRepository
    {
        public T_EmployeeRepository(VINASICEntities dataContext) : base(dataContext)
    	{
    	
    	}
    
    	public T_EmployeeRepository(IUnitOfWork<VINASICEntities> unitOfWork) : base(unitOfWork.DataContext)
    	{
    	
    	}
        
    }
    
    public interface IT_EmployeeRepository : IRepository<T_Employee>
    {
    }
    
}
