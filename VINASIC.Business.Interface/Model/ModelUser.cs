﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using VINASIC.Object;

namespace VINASIC.Business.Interface.Model
{
    public class ModelUser : T_User
    {
        public List<ModelUserRole> UserRoles { get; set; }

        public string Address { get; set; }
        public string Mobile { get; set; }
        public DateTime HireDate { get; set; }
        public string Position { get; set; }
    }
}
