﻿using System;
using PagedList;
using System.Collections.Generic;
using VINASIC.Business.Interface.Model;
namespace VINASIC.Business.Interface
{
    public interface IBllOrder
    {
        PagedList<ModelOrder> GetList(int listEmployee, int startIndexRecord, int pageSize, string sorting,string fromDate,string toDate);
        List<ModelOrderDetail> GetListOrderDetailByOrderId(int orderId);
        ResponseBase UpdateApproval(int orderId,int id, int userId);
        ResponseBase UpdatePayment(int orderId,int id, int userId);
        ResponseBase UpdateHasTax(int orderId, int id, int userId);
        ResponseBase UpdatedOrder(ModelSaveOrder obj,int userId);
        ResponseBase CreateOrder(ModelSaveOrder obj,int userId);
        ResponseBase UpdatePrintUser(int detailId, int printId,string description);
        ResponseBase UpdateHaspay(int orderId,string haspay);
        ResponseBase UpdateDesignUser(int detailId, int printId,string description);
        List<ModelExportOrder> ExportReport(DateTime fromDate, DateTime toDate);
    }
}