﻿using System.Collections.Generic;
using PagedList;
using VINASIC.Business.Interface.Model;
using VINASIC.Object;

namespace VINASIC.Business.Interface
{
    public interface IBLLPermission 
    {
         ResponseBase Create(T_Permission obj);
        ResponseBase Update(T_Permission obj);
        ResponseBase DeleteById(int id, int userId);
        ResponseBase DeleteByListId(List<int> listId, int userId);
        ResponseBase GetListPermissionByListRoleId(List<int> listRoleId);
        List<T_Permission> GetListPermissionByFeatureID(int FeatureId);         
        PagedList<T_Permission> GetList(int counTryId, int startIndexRecord, int pageSize, string sorting);
    }
}
