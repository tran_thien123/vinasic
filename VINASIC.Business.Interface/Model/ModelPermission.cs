﻿using VINASIC.Object;

namespace VINASIC.Business.Interface.Model
{
    public class ModelPermission : T_Permission
    {
        public int moduleId { get; set; }
    }
}
