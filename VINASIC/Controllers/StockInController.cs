﻿using Dynamic.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;

namespace VINASIC.Controllers
{
    public class StockInController : BaseController
    {
        private readonly IBllStockIn _bllStockIn;
        private readonly IBllMaterial _material;
        private readonly IBllMaterialType _materialType;
        private readonly IBllPartner _bllPartner;

        public StockInController(IBllStockIn bllStockIn, IBllPartner bllPartnerr, IBllMaterialType materialType, IBllMaterial material)
        {
            _bllStockIn = bllStockIn;
            _bllPartner = bllPartnerr;
            _materialType = materialType;
            _material = material;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetStockIns(int jtStartIndex = 0, int jtPageSize = 10, string jtSorting = "")
        {
            try
            {

                var listStockIn = _bllStockIn.GetList(UserContext.IsOwner ? UserContext.UserID : 1, jtStartIndex, jtPageSize, jtSorting);
                JsonDataResult.Records = listStockIn;
                JsonDataResult.Result = "OK";
                JsonDataResult.TotalRecordCount = listStockIn.TotalItemCount;

            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }
        public JsonResult ListStockInDetail(int stockInId)
        {
            try
            {
                Thread.Sleep(200);
                var stockInDetail = _bllStockIn.GetListStockInDetailByStockInId(stockInId);
                return Json(new { Result = "OK", Records = stockInDetail });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult SaveStockIn(int stockInId, string description, int customerId, string customerName, string dateDelivery,float orderTotal, List<ModelStockDetail> listDetail)
        {
            try
            {
                var saveStockIn = new ModelSaveStockIn
                {                 
                    Description = description,
                    StockInId = stockInId,
                    OrderTotal = orderTotal,
                    PartnerId = customerId,
                    CustomerName = customerName,
                    DateDelivery = DateTime.Parse(dateDelivery ?? DateTime.Now.ToString(CultureInfo.InvariantCulture)),
                    Detail = listDetail
                };
                if (!IsAuthenticate)
                {
                    var responseResult = saveStockIn.StockInId == 0 ? _bllStockIn.CreateStockIn(saveStockIn, UserContext.UserID) : _bllStockIn.UpdatedStockIn(saveStockIn, UserContext.UserID);
                    if (!responseResult.IsSuccess)
                    {
                        JsonDataResult.Result = "ERROR";
                        JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                    }
                    else
                    {
                        JsonDataResult.Result = "OK";
                    }
                }
            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update ", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        [HttpPost]
        public JsonResult GetAllCustomer()
        {
            try
            {
                Thread.Sleep(200);
                var customers = _bllPartner.GetListPartner();
                return Json(new { Result = "OK", Records = customers });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult GetListPartner()
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _bllPartner.GetListPartner();
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult GetListProductType()
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _materialType.GetListMaterialType();
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult GetListProduct(int productType)
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _material.GetListMaterial(productType);
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }
        public JsonResult GetPartnerById(int partId)
        {
            try
            {
                Thread.Sleep(200);
                var customer = _bllPartner.GetPartnerById(partId);
                return Json(new { Result = "OK", Records = customer });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
