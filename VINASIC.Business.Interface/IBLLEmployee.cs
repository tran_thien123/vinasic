﻿using PagedList;
using System.Collections.Generic;
using VINASIC.Business.Interface.Model;
using VINASIC.Object;

namespace VINASIC.Business.Interface
{
    public interface IBllEmployee
    {
        ResponseBase Create(ModelEmployee obj);
        ResponseBase Update(ModelEmployee obj);
        ResponseBase DeleteById(int id, int userId);
        ResponseBase DeleteByListId(List<int> listId, int userId);
        PagedList<ModelEmployee> GetList(string keyWord, int startIndexRecord, int pageSize, string sorting);
        List<ModelSelectItem> GetListEmployee();
        List<ModelSelectItem> GetCustomerByOrganization(int organId);
        PagedList<ModelForDesign> GetListForDesign(string keyWord, int startIndexRecord, int pageSize, string sorting,int userid);
        PagedList<ModelForPrint> GetListForPrint(string keyWord, int startIndexRecord, int pageSize, string sorting,int userid);
        ResponseBase DesignUpdateOrderDeatail(int id, int stautus, int userId);
        ResponseBase PrintUpdateOrderDeatail(int id, int stautus, int userId);
        ResponseBase BusinessUpdateOrderDeatail(int id, int employeeId, string description, int type, int stautus,int userId);
    }
}
