using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dynamic.Framework.Infrastructure.Data;
using VINASIC.Object;

namespace VINASIC.Data.Repositories
{
    public partial class T_CommodityRepository : RepositoryBase<VINASICEntities,T_Commodity> , IT_CommodityRepository
    {
        public T_CommodityRepository(VINASICEntities dataContext) : base(dataContext)
    	{
    	
    	}
    
    	public T_CommodityRepository(IUnitOfWork<VINASICEntities> unitOfWork) : base(unitOfWork.DataContext)
    	{
    	
    	}
        
    }
    
    public interface IT_CommodityRepository : IRepository<T_Commodity>
    {
    }
    
}
