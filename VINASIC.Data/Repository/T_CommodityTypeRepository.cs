using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dynamic.Framework.Infrastructure.Data;
using VINASIC.Object;

namespace VINASIC.Data.Repositories
{
    public partial class T_CommodityTypeRepository : RepositoryBase<VINASICEntities,T_CommodityType> , IT_CommodityTypeRepository
    {
        public T_CommodityTypeRepository(VINASICEntities dataContext) : base(dataContext)
    	{
    	
    	}
    
    	public T_CommodityTypeRepository(IUnitOfWork<VINASICEntities> unitOfWork) : base(unitOfWork.DataContext)
    	{
    	
    	}
        
    }
    
    public interface IT_CommodityTypeRepository : IRepository<T_CommodityType>
    {
    }
    
}
