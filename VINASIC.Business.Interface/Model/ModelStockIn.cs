﻿using VINASIC.Object;

namespace VINASIC.Business.Interface.Model
{
    public class ModelStockIn : T_StockIn
    {
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerTaxCode { get; set; }
    }
}

