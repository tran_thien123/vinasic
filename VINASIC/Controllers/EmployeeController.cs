﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using VINASIC.Business.Interface;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using VINASIC.Business.Interface.Model;
using VINASIC.Hubs;
using Dynamic.Framework.Mvc;
namespace VINASIC.Controllers
{
    public class EmployeeController : BaseController
    {
        private readonly IBllEmployee _bllEmployee;
        private readonly IBllPosition _bllPosition;
        public EmployeeController(IBllEmployee bllEmployee, IBllPosition bllPosition)
        {
            _bllEmployee = bllEmployee;
            _bllPosition = bllPosition;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DesignIndex()
        {
            return View();
        }

        public ActionResult PrintIndex()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetEmployees(string keyword, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {

                var listEmployee = _bllEmployee.GetList(keyword, jtStartIndex, jtPageSize, jtSorting);
                JsonDataResult.Records = listEmployee;
                JsonDataResult.Result = "OK";
                JsonDataResult.TotalRecordCount = listEmployee.TotalItemCount;

            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }

        [HttpPost]
        public JsonResult GetJobForDesign(string keyword, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {

                var listEmployee = _bllEmployee.GetListForDesign(keyword, jtStartIndex, jtPageSize, jtSorting,UserContext.employeeId);
                JsonDataResult.Records = listEmployee;
                JsonDataResult.Result = "OK";
                JsonDataResult.TotalRecordCount = listEmployee.TotalItemCount;

            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }
        [HttpPost]
        public JsonResult GetJobForPrint(string keyword, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            try
            {
                var listEmployee = _bllEmployee.GetListForPrint(keyword, jtStartIndex, jtPageSize, jtSorting, UserContext.employeeId);
                JsonDataResult.Records = listEmployee;
                JsonDataResult.Result = "OK";
                JsonDataResult.TotalRecordCount = listEmployee.TotalItemCount;

            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }

        [HttpPost]
        public JsonResult SaveEmployee(ModelEmployee modelEmployee)
        {
            try
            {
                if (!IsAuthenticate)
                {
                    ResponseBase responseResult;
                    if (modelEmployee.Id == 0)
                    {
                        modelEmployee.CreatedUser = UserContext.UserID;
                        responseResult = _bllEmployee.Create(modelEmployee);
                    }
                    else
                    {
                        modelEmployee.UpdatedUser = UserContext.UserID;
                        responseResult = _bllEmployee.Update(modelEmployee);
                    }
                    if (!responseResult.IsSuccess)
                    {
                        JsonDataResult.Result = "ERROR";
                        JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                    }
                    else
                    {
                        JsonDataResult.Result = "OK";
                    }
                }

            }
            catch (Exception ex)
            {
                //add error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Update ", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }

        [HttpPost]
        public JsonResult DeleteEmployee(int id)
        {
            try
            {
                if (!IsAuthenticate)
                {
                    var responseResult = _bllEmployee.DeleteById(id, UserContext.UserID);
                    if (responseResult.IsSuccess)
                        JsonDataResult.Result = "OK";
                    else
                    {
                        JsonDataResult.Result = "ERROR";
                        JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                    }
                }
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Delete BankBranch", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }

        [HttpPost]
        public JsonResult DesignUpdateOrderDeatail(int id,int status)
        {
            try
            {
                var result = _bllEmployee.DesignUpdateOrderDeatail(id, status, UserContext.UserID);
                JsonDataResult.Records = result;
                JsonDataResult.Result = JsonDataResult.Result = result.IsSuccess ? JsonDataResult.Result = "OK" : JsonDataResult.Result = "ERROR";

            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }
        [HttpPost]
        public JsonResult PrintUpdateOrderDeatail(int id, int status)
        {
            try
            {
                var result = _bllEmployee.PrintUpdateOrderDeatail(id, status, UserContext.UserID);
                JsonDataResult.Records = result;
                JsonDataResult.Result = result.IsSuccess ? JsonDataResult.Result = "OK" : JsonDataResult.Result = "ERROR";
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });

            }
            return Json(JsonDataResult);
        }
        public JsonResult GetEmployeePosition()
        {
            try
            {
                List<SelectListItem> listValues = new List<SelectListItem>();
                var listProductType = _bllPosition.GetListPosition();
                if (listProductType != null)
                {
                    listValues = listProductType.Select(c => new SelectListItem()
                    {
                        Value = c.Value.ToString(),
                        Text = c.Name
                    }).ToList();
                }
                return Json(listValues, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }

    }
}
