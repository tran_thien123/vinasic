﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VINASIC.Object;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;
using VINASIC.Controllers;
using Dynamic.Framework.Mvc;

namespace SystemAccount.Controllers
{
    public class RoleController : BaseController
    {
        private readonly IBLLRole ibllRole;
        private readonly IBLLUserRole bllUserRole;
        private readonly IBLLRolePermission bllRolePermission;
        public RoleController(IBLLRole _ibllRole, IBLLRolePermission _bllRolePermission, IBLLUserRole _bllUserRole)
        {
            this.ibllRole = _ibllRole;
            this.bllRolePermission = _bllRolePermission;
            this.bllUserRole = _bllUserRole;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetRoles(string keyWord, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "")
        {
            try
            {
                if (!IsAuthenticate)
                {
                    var roles = ibllRole.GetListRole(keyWord, jtStartIndex, jtPageSize, jtSorting, UserContext.UserID, UserContext.CompanyID ?? 0, UserContext.IsOwner);
                    JsonDataResult.Records = roles;
                    JsonDataResult.Result = "OK";
                    JsonDataResult.TotalRecordCount = roles.TotalItemCount;
                }
            }
            catch (Exception ex)
            {
                //add Error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Get List ObjectType", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }


        public ActionResult CreateRole(int? id)
        {
            try
            {
                ViewData["Features"] = ibllRole.GetListFeatureByUserId(UserContext.UserID);
                ViewData["Permissions"] = ibllRole.GetListPermissionByUserId(UserContext.UserID);
                int ID = id ?? 0; // nullable  default value is 0;
                if (ID != 0)
                {
                    var roleDetail = ibllRole.GetRoleDetailByRoleId(ID);
                    if (roleDetail != null)
                    {
                        ViewData["RoleDetail"] = roleDetail;
                        ViewData["RolePermission"] = ibllRole.GetListRolePermissionByRoleId(ID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }

        [HttpPost]
        public JsonResult Save(int id, string roleName, string description, List<string> listPermission)
        {
            ResponseBase result;
            try
            {
                if (!IsAuthenticate)
                {
                    T_RoLe role = new T_RoLe();
                    role.Id = id;
                    role.RoleName = roleName;
                    role.IsSystem = false;
                    role.CreatedUser = UserContext.UserID;
                    role.CreatedDate = DateTime.Now;
                    role.Decription = description;
                    if (id == 0)
                    {
                        result = ibllRole.Create(role, listPermission);
                    }
                    else
                    {
                        result = ibllRole.Update(role, listPermission);
                    }
                    if (result.IsSuccess)
                    {
                        JsonDataResult.Result = "OK";
                    }
                    else
                    {
                        JsonDataResult.Result = "ERROR";
                        JsonDataResult.ErrorMessages.AddRange(result.Errors);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(JsonDataResult);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            ResponseBase responseResult;
            try
            {
                if (!IsAuthenticate)
                {
                    responseResult = new ResponseBase();
                    responseResult = ibllRole.DeleteById(id, UserContext.UserID);
                    if (!responseResult.IsSuccess)
                    {
                        JsonDataResult.Result = "ERROR";
                        JsonDataResult.ErrorMessages.AddRange(responseResult.Errors);
                    }
                    JsonDataResult.Result = "OK";
                }
            }
            catch (Exception ex)
            {
                //add Error
                JsonDataResult.Result = "ERROR";
                JsonDataResult.ErrorMessages.Add(new Error() { MemberName = "Lỗi Dữ Liệu", Message = "Lỗi: " + ex.Message });
            }
            return Json(JsonDataResult);
        }

    }
}
