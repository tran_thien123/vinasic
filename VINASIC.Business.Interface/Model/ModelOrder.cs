﻿using System;
using VINASIC.Object;

namespace VINASIC.Business.Interface.Model
{
    public class ModelOrder : T_Order
    {
        public string strIspayment { get; set; }
        public string strIsApproval { get; set; }
        public string strSubTotal { get; set; }
        public string CreateUserName { get; set; }
        public double PrintfPecent { get; set; }
        public string strHaspay { get; set; }
        public string strHasTax { get; set; }


        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerTaxCode { get; set; }
    }

    public class ModelExportOrder
    {
        public DateTime NgayTao { get; set; }
        public string TenKhachHang { get; set; }
        public string TenDichVu { get; set; }
        public string MoTa { get; set; }
        public double ChieuDai { get; set; }
        public double ChieuRong { get; set; }
        public double DienTich { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }        
    }
}

