﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dynamic.Framework;
using Dynamic.Framework.Infrastructure.Data;
using PagedList;
using VINASIC.Business.Interface;
using VINASIC.Business.Interface.Model;
using VINASIC.Data;
using VINASIC.Data.Repositories;
using VINASIC.Object;

namespace SystemAccount.Bussiness
{
    public class BLLUserRole : IBLLUserRole
    {
        private readonly IT_UserRoleRepository repUserRole;
        private readonly IT_RoLeRepository repRole;
        private readonly IUnitOfWork<VINASICEntities> unitOfWork;
        public BLLUserRole(IUnitOfWork<VINASICEntities> _unitOfWork, IT_UserRoleRepository _repUserRole, IT_RoLeRepository _repRole)
        {
            this.unitOfWork = _unitOfWork;
            this.repUserRole = _repUserRole;
            this.repRole = _repRole;
        }
        
        private void SaveChange()
        {
            try
            {
                this.unitOfWork.Commit();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ResponseBase Create(ModelUserRole asset)
        {
            throw new NotImplementedException();
        }

        public ResponseBase Update(ModelUserRole asset)
        {
            throw new NotImplementedException();
        }

        public ResponseBase DeleteById(int id, int userId)
        {
            throw new NotImplementedException();
        }

        public ResponseBase DeleteByListRoleId(List<int> listRoleId, int userId, int acctionUserId)
        {
            var result = new ResponseBase();
            try
            {                
                var listUserRole = repUserRole.GetMany(x => !x.IsDeleted && listRoleId.Contains(x.RoleId) && x.UserId == userId );
                if (listRoleId.Count > 0)
                {
                    foreach (var item in listUserRole)
                    {
                        item.IsDeleted = true;
                        item.DeletedDate = DateTime.Now;
                        item.DeletedUser = acctionUserId;
                        repUserRole.Update(item);
                    }
                    SaveChange();
                    result.IsSuccess = true;                    
                }
                return result;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                throw ex;
            }
        }

        public PagedList<ModelUserRole> GetList(int counTryId, int startIndexRecord, int pageSize, string sorting)
        {
            throw new NotImplementedException();
        }

        public List<int> GetUserRolesIdByUserId(int userId)
        {
            List<int> userRolesId = null;
             try
            {
                userRolesId = repUserRole.GetMany(x => !x.IsDeleted && x.UserId == userId).Select(x => x.Id ).ToList();                                 
            }
            catch (Exception ex)
            {
                throw ex;
            }
             return userRolesId;
        }

        public List<ModelSelectItem> GetUserRolesModelByUserId(int userId, bool IsOwner,int companyId)
        {
            List<ModelSelectItem> roles = null;
            try
            {
                if (IsOwner)
                {
                    roles = repRole.GetMany(x => !x.IsDeleted).Select(x => new ModelSelectItem()
                    {
                        Name = x.RoleName,
                        Value = x.Id
                    }).ToList();
                }
                else
                {
                    roles = repUserRole.GetMany(x => !x.IsDeleted && x.UserId == userId).Select(x => new ModelSelectItem()
                    {
                        Name = x.T_RoLe.RoleName,
                        Value = x.Id
                    }).ToList();
                }
                
                return roles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseBase AddListUserRoleByListRoleIdAndUserId(List<int> listRoleId, int UserId, int acctionUserId)
        {
            try
            {
                var result = new ResponseBase();

                if (listRoleId.Count > 0)
                {
                    foreach (var item in listRoleId)
                    {
                        var userRole = new T_UserRole();
                        userRole.RoleId = item;
                        userRole.UserId = UserId;
                        userRole.CreatedDate = DateTime.Now;
                        userRole.CreatedUser = acctionUserId;
                        repUserRole.Add(userRole);
                    }
                    SaveChange();
                    result.IsSuccess = true;
                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
